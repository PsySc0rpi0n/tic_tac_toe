#include "jg_common.h"

/*===========================================================================================*
*  Protótipos das funções até agora usadas                                                   *
*============================================================================================*/

void m_menu();
void reg_jogador();
void reg_jog();
void mostra_info();
void apaga_jogador();
void jogar();
void list_pont();
void jog_jj();
void jog_jc();
void jog_cc();
void jog_mc();
void jog_ac();
void final_jogo();

PlData PedeDadosJogador();
void GravaDadosBin();
PlData *LeDadosBin();

char m_menu_opcao(char *op);

void iniciar_matrix();
void coord();
void coord1_pc(int *n1,int *n2);
void coord2_pc();
int verifica_vencedor();
void apresenta_matrix();
void Random(int *m1, int *m2); 

/*===========================================================================================*
*  Fim                                                                                       *
*============================================================================================*/

char m_menu_opcao (char *op)
{
#ifdef USE_FGETS
    char buffer[BUFFER_SIZE];

    if (!fgets(buffer,      // local de memória onde se irá guardar o que foi lido do stdin
               BUFFER_SIZE, // tamanho máximo do buffer que irá guardar a informação lida
               stdin))      // local onde a informação irá ser lida
    {
        return 0;           // retornar valor que sinaliza erro de leitura
    }

    return sscanf(buffer,    // buffer usado para a leitura formatada dos dados
                  "%c",      // formato dos dados a serem lidos
                  op);       // local onde os dados irão ser guadados 
#else
    int ok;

    ok = scanf ("%c",        // formato dos dados a serem lidos
                op);         // local onde os dados irão ser guadados 
    CLEAR_INPUT;             // chamada da macro de limpeza dos dados no buffer de entrada

    return ok;               // retornar o numero de valores lidos do scanf (0 ou 1)
#endif
}



void m_menu () {
   char op;
   int ok;

   do {
      CLEAR_SCREEN;
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      printf ("O Jogo do Galo\n");
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      printf ("\tMenu Principal\n");
      printf ("\n");
      printf ("\t\t(R)egistar Novo Jogador\n");
      printf ("\t\t(M)ostrar informação sobre o Jogador\n");
      printf ("\t\t(A)pagar Jogador\n");
      printf ("\t\t(J)ogar\n");
      printf ("\t\t(L)istar Pontuações\n");
      printf ("\t\t(S)air\n");
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      do{
         printf ("\nEscolha o menu: ");
         fflush(stdout);
         ok = m_menu_opcao(&op);
         if (ok) {
            switch (tolower (op)) {
               case 'r':
                  reg_jogador ();
                  break;
               case 'm':
                  mostra_info ();
                  break;
               case 'a':
                  apaga_jogador ();
                  break;
               case 'j':
                  jogar ();
                  break;
               case 'l':
                  list_pont ();
                  break;
               case 's':
                  break;
               default:
                  printf ("Opção inválida!\n");
                  ok = 0;
                  break;
            }
         }
      } while (!ok);
   } while (op != 's');
}

void reg_jogador () {
   char op;
   int ok;

   do{
      CLEAR_SCREEN;
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      printf ("O Jogo do Galo\n");
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      printf ("\tMenu Registar Jogador\n");
      printf ("\n");
      printf ("\t\t(R)egistar Novo Jogador\n");
      printf ("\t\t(S)air\n");
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      do{
         printf ("\t\tEscolha o menu: \n");
         ok = m_menu_opcao(&op);
         if (ok) {
            switch (tolower (op)) {
               case 'r':
                  reg_jog ();
                  break;
               case 's':
                  break;
               default:
                  printf ("Opção inválida!\n");
                  ok = 0;
                  break;
            }
         }
      } while (!ok);
   } while (op != 's');
}
 
PlData PedeDadosJogador(){
  PlData pl_data;

  printf ("Nick de jogador:\n");
  fgets (pl_data.nome_jog, sizeof (pl_data.nome_jog), stdin);
  printf ("Nome completo:\n");
  fgets (pl_data.nome_user, sizeof (pl_data.nome_user), stdin);
  printf ("Introduza o género \"Masculino/Feminino\":\n");
  fgets (pl_data.genero, sizeof (pl_data.genero), stdin);
  printf ("Data de nascimento:\n");
  pldata.pvic = 0;
  pldata.pemp = 0;
  pldata.pder = 0;
  do {
    ok = fscanf (stdin, "%d-%d-%d", &pl_data.data_nasc.dia, &pl_data.data_nasc.mes, &pl_data.data_nasc.ano);
    if (ok != 3){
      printf ("Introduza a data neste formato [dd-mm-aaaa]:\n");
      CLEAR_INPUT;
    }
  } while (ok != 3);

  return pl_data;
}


/////////////////Função para ler dados do ficheiro binário//////////////////
PlData *LeDadosBin(unsigned int *strct){
  FILE *binf;
  int el;
  unsigned int i;
  PlData *pldatatmp;
  
  if ((binf = fopen (PL_BIN_FILE, "rb")) == NULL) {
    printf ("Erro ao abrir o ficheiro!\n");
    exit (0);
  }

  fread (&strct, sizeof (unsigned int), 1, binf);

///////////////////Para controlo/////////////////
  printf ("Struct:%u\n", strct);
////////////////////////////////////////////////
  
  if ((pldatatmp = malloc (strct*sizeof (*pldatatmp))) == NULL) {
    printf ("Erro de memória!\n");
    exit (0);
  }//Memória libertada no fim da função main

  for (i = 0; i < strct; i++){
    fread ((pldatatmp + i), sizeof (*pldatatmp), 1, binf);
    printf ("%u\n", i);
  }
  
///////////////Para controlo//////////////////////
  printf ("%d elementos lidos!\n", i);

  for (i = 0; i < strct; i++) {
    printf("Nome do jogador: %s\n", pldatatmp[i].nome_jog);
    printf("O nickname é: %s\n", pldatatmp[i].nome_user);
    printf("O género do jogador é: %s\n", pldatatmp[i].genero);
    printf("Número de vitórias: %d\nNúmero de empates:%d\nNúmero de derrotas:%d\n", pldatatmp[i].pvic, pldatatmp[i].pemp, pldatatmp[i].pder);
    printf("A data de nascimento é: %d-%d-%d\n", pldatatmp[i].data_nasc.dia, pldatatmp[i].data_nasc.mes, pldatatmp[i].data_nasc.ano);
  }
//////////////////////////////////////////////////

  fclose (binf);
  return *pldatatmp;
}


//////////////////Procurar dados do jogador//////
int ProcuraDadosJogador(PlData *pldatatmps, PlData pl_datas){
  unsigned int *strct;
  pldatatmps = LeDadosBin(strct);
  pl_datas = PedeDadosJogador();



}

void reg_jog () {
   /*
   char op;
   int ok;

   do{
      CLEAR_SCREEN;
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      printf ("O Jogo do Galo\n");
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      printf ("\t\t(S)air\n");
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");

      do{
         printf ("\t\tEscolha o menu: \n");
         ok = m_menu_opcao(&op);
         if (ok) {
            switch (tolower (op)) {
            case 's':
               break;
            default:
               printf ("Opção inválida!\n");
               ok = 0;
               break;
            }
         }
      } while (!ok);
    } while (op != 's');
  */


}
 
 
void mostra_info (){
   char op;
   int ok;

   do {
      CLEAR_SCREEN;
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      printf ("\nO Jogo do Galo \n");
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      printf ("\tRegistar Jogador\n");
      printf ("\n");
      printf ("\t\t(S)air\n");
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      do {
         printf ("\t\tEscolha o menu: \n");
         ok = m_menu_opcao(&op);
         if (ok) {
            switch (tolower (op)) {
               case 's':
                  break;
               default:
                  printf ("Opção inválida!\n");
                  ok = 0;
                  break;
            }
         }
      } while (!ok);
   } while (op != 's');
}
 
void apaga_jogador () {
   char op;
   int ok;

   do {
      CLEAR_SCREEN;
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      printf ("O Jogo do Galo\n");
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      printf ("\tMenu Apagar Jogador\n");
      printf ("\n");
      printf ("\t\t(S)air\n");
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");

      do {
         printf ("\t\tEscolha o menu: \n");
         ok = m_menu_opcao(&op);
         if (ok) {
            switch (tolower (op)) {
               case 's':
                  break;
               default:
                  printf ("Opção inválida!\n");
                  ok = 0;
                  break;
            }
         }
      } while (!ok);
   } while (op != 's');
}
 
void jogar () {
   char op;
   int ok;
   
   do {
      CLEAR_SCREEN;
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      printf ("\nO Jogo do Galo \n");
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      printf ("\tMenu Jogar\n");
      printf ("\n");
      printf ("\t\tJogador vs (C)omputador\n");
      printf ("\t\t(J)ogador vs Jogador\n");
      printf ("\t\tComp(u)tador vs Computador\n");
      printf ("\t\t(S)air\n");
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");

      do {
         printf ("\t\tEscolha o menu: \n");
         ok = m_menu_opcao(&op);
         if (ok) {
            switch (tolower (op)) {
               case 'c':
                  jog_jc ();
                  final_jogo ();
		  break;
               case 'j':
                  jog_jj ();
                  break;
               case 'u':
                  printf ("%c", op);
			jog_cc ();
                    	break;
                case 's':
                    	break;
                default:
                    	printf ("Opção inválida!\n");
                    	ok = 0;
                    	break;
                }
            }
        } while (!ok);
    } while (op != 's');
}

void final_jogo () {
   char op;
   int ok;

   do {
      CLEAR_SCREEN;
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      printf ("\nO Jogo do Galo \n");
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      printf ("\tMenu Jogar de novo\n");
      printf ("\n");
      printf ("\t\tJogar novamente nas (m)esmas condições (mesmos adversários)\n");
      printf ("\t\tJogar novamente mas al(t)erar condições\n\n");
      printf ("\t\t(S)air\n");
      printf ("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      
      do {
         printf ("\t\tEscolha o menu: \n");
         ok = m_menu_opcao (&op);
         if (ok) {
            switch (tolower (op)) {
               case 'm':
                     jog_mc ();
                     break;
               case 't':
                     jog_ac ();
                     break;
               case 's':
                     break;
                     default:
                     printf ("Opção inválida!\n");
                     ok = 0;
                     break;
            }
         }
      } while (!ok);
   } while (op != 's');
}

void jog_mc () {
   CLEAR_SCREEN;

   jog_jc ();
}

void jog_ac () {
	CLEAR_SCREEN;
	printf ("\t\tMenu jogar com novas condições\n\n\n");

	printf ("Prima ENTER para continuar");
	getchar ();
}

void list_pont () {
    CLEAR_SCREEN;
    printf ("\t\tMenu listar pontuações\n\n\n");

    printf ("\t\tPrima ENTER para continuar");
    getchar ();
}
 
void jog_jc (){
   CLEAR_SCREEN;
   int *p1 = NULL; 
   int *p2 = NULL;
   int PCfirst = 0;
   int k1, k2;
   
   int contaJogadas = 0;
   int ok = 0;

   p1 = &k1;
   p2 = &k2;
   
   printf("\t\t\t\t Jogo do Galo \n\n\n");
   
   iniciar_matrix();
   
   apresenta_matrix();
   
   printf("\n");

   do {

      contaJogadas++;
      coord();
      
      //Verifica o vencedor após a Jogada do Humano
      ok = verifica_vencedor ();
      if (ok == 1){
         printf("\n\n Você venceu!\n\n");
         break;
      }
      
      if(PCfirst == 0){
         Random(p1, p2); // call the function and send the address of p1 and p2 (&)(&)
         coord1_pc(p1, p2); 
         PCfirst = 1;
      }
      else
         coord2_pc();
        
         //Verifica o vencedor após a jogada do Computador
         
      ok = verifica_vencedor();
      if (ok == 1) {
         printf("\n\n O computador ganhou!\n\n");
         break;
      }
      
      apresenta_matrix();

      if (contaJogadas == 5 && ok == 0){
         printf ("\n\nJogo empatado!\n\n");
         break;
      }
    
   }while(1);

   // Apresentar a Matriz após a verificação do vencedor
   apresenta_matrix();
   CLEAR_INPUT;
   getchar(); //implica 2 enters no fim do jogo
}

// Função que Incializa a matrix
void iniciar_matrix(){
   int i, j;

   for(i = 0; i < DIM; i++){
      for (j = 0; j < DIM; j++){
         matrix[i][j] = ' ';
      }
   }
}

//Função para a coordenadas do jogador
void coord(){
   unsigned int x=0, y=0;

   //printf("Make use of space to separate these.\n");
   printf ("Introduza as coordenadas X e Y, separadas por um espaço:\n");
   if ((scanf ("%u \t %u", &x, &y)) != 2) {
   
   }
      
   CLEAR_INPUT;

   while ((x < 1 || x > 3) || (y < 1 || y > 3)){
      printf ("Coordenada X=%d\tCoordenada Y=%d", x, y); //debugging option
      printf ("\nIntroduziu coordenadas inválidas!\n");
      printf ("Introduza as coordenadas X e Y, separadas por um espaço:\n");
      x = 0;
      y = 0;
      if ((scanf ("%u \t %u", &x, &y)) != 2) {
      
      }
         
      CLEAR_INPUT;
   }
   
   x--;
   y--;

   if (matrix[x][y] != ' '){
      printf("\n Posição Ocupada! \n");
      coord();
   }
   else {
      matrix[x][y] = 'X';
   }
}

//Função para apresentar a matrix

void apresenta_matrix(){
   int i;

   printf("\n");

   for (i = 0; i < DIM; i++){
      printf(" %c | %c | %c ", matrix[i][0], matrix[i][1], matrix[i][2]);
      if ( i != 2){
         printf("\n");
         printf("---|---|---");
         printf("\n");
      }
   }

   printf("\n");
}

// Função para a primeira Jogada do Computador

void coord1_pc(int *p1,int *p2){
   int i, j;
   i = *p1;
   j = *p2;

   for (i = 0; i < DIM; i++){
      for(j = 0; j < DIM; j++){
         if (matrix[i][j] == ' '){
            matrix[i][j] = 'O';
            return;
         }
      }
   }
}

//Função para as restantes jogadas do computador

void coord2_pc(){
   int i, j;

   for (i = 0; i < DIM; i++){
      for(j = 0; j < DIM; j++){
         if (matrix[i][j] == ' '){
            matrix[i][j] = 'O';
            return;
         }
      }
   }
}

//Função para verificar o Vencedor do jogo

int verifica_vencedor(){
   int i;

   // Verificar Linhas e retorna o valor 1 caso seja verdadeiro
   for (i = 0; i < DIM; i++){
      if (matrix[i][0] != ' ' && matrix[i][1] != ' ' && matrix[i][2] != ' '){
         if(matrix[i][0] == matrix[i][1] && matrix[i][0] == matrix[i][2]){
            return 1;
         }
      }
   }

   // Verificar colunas e retorna o valor 1 caso seja verdadeiro

   for (i = 0; i < DIM; i++){
      if (matrix[0][i] != ' ' && matrix[1][i] != ' ' && matrix[2][i] != ' '){
         if (matrix[0][i] == matrix[1][i] && matrix[0][i] == matrix[2][i]){
            return 1;
         }
      }
   }

   // Verificar as diagonais e retornar o valor 1 caso seja verdadeiro

   if(matrix[0][0] != ' ' && matrix[1][1] != ' ' && matrix[2][2] != ' '){
      if (matrix[0][0] == matrix[1][1] && matrix[0][0] == matrix[2][2]){
         return 1;
      }
   }

   if(matrix[0][2] != ' ' && matrix[1][1] != ' ' && matrix[2][0] != ' '){
      if(matrix[0][2] == matrix[1][1] && matrix[0][2] == matrix[2][0]){
         return 1;
      }
   }
return 0;
}

void Random(int *m1, int *m2){ 

   // This function generate de random position on case of this be necessary!
   // Receive te pointers of variables on the main
   int x, y, i;
   
   for( i = 2; i > 0; i--){
      //Because the time generate a rnd each timc second 
      //changed, i make use of a combination of rnd generators.
      srand((unsigned)time(NULL));
      x = (rand() % 3 + 1);
      //x = rand() % 4;
      //if (x==0) 
      // x++;
      srand((unsigned)time(NULL));
      //y = (rand() % 3 + 1);
      y = rand() % 4;
      if (y == 0) 
         y++;
      //printf("random numbers = %d %d \n", x , y);
      *m1 = x; // Address of n1 catch the value present on x
      *m2 = y; // Address of n1 catch the value present on y
   }
}
 
void jog_jj () {
   CLEAR_SCREEN;
   printf ("\t\tComeça o jogo b!\n\n\n");
   
   printf ("\t\tPrima ENTER para continuar");
   getchar ();
   final_jogo ();
}
 
void jog_cc () {
   CLEAR_SCREEN;
   printf ("\t\tComeça o jogo c!\n\n\n");

   printf ("\t\tPrima ENTER para continuar");
   getchar ();
   final_jogo ();
}

int main () {
    m_menu ();
    free (pldatatmp);
    return 0;
}
