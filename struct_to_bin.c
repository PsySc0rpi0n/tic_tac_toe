#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_NOM 16
#define MAX_USER 56
#define MAX_GEN 10

#define CLEAR_INPUT while (getchar ()!= '\n')
#define PL_BIN_FILE "player_data.bin"


typedef struct {
   char nome_jog [MAX_NOM];
   char nome_user [MAX_USER];
   char genero [MAX_GEN];
   int pvic;
   int pdemp;
   int pder;
   struct datan{
      int dia;
      int mes;
      int ano;
   } data_nasc;
} PlData;

int main (void) {
	
   FILE *binf;
   int ok, w_el, readstrn = 0; 
   unsigned int strct = 0;
   PlData pl_data;

   printf ("Nick de jogador:\n");
   fgets (pl_data.nome_jog, sizeof (pl_data.nome_jog), stdin);
   printf ("Nome completo:\n");
   fgets (pl_data.nome_user, sizeof (pl_data.nome_user), stdin);
   printf ("Introduza o género \"Masculino/Feminino\":\n");
   fgets (pl_data.genero, sizeof (pl_data.genero), stdin);
   printf ("Data de nascimento:\n");
   do {
      ok = fscanf (stdin, "%d-%d-%d", &pl_data.data_nasc.dia, &pl_data.data_nasc.mes, &pl_data.data_nasc.ano);
      if (ok != 3){
         printf ("Introduza a data neste formato [dd-mm-aaaa]:\n");
         CLEAR_INPUT;
      }
   } while (ok != 3);

   if ((binf = fopen (PL_BIN_FILE, "r+b")) == NULL) {//abre o ficheiro em modo escrita/leitura
      if ((binf = fopen (PL_BIN_FILE, "wb")) == NULL){//se não conseguiu, cria o ficheiro
         printf ("Não foi possível criar/abrir ficheiro!\n");
         exit (0);
      }
   }

   if ((readstrn = fread (&strct, sizeof (unsigned int), 1, binf)) == 0) {//verifica se o ficheiro não contém o contador
      //printf ("readstrn=%d\n", readstrn); debugging
      //printf ("Counter: %d\n", strct); debugging
      strct = 1;
      //printf ("Counter: %d\n", strct);debugging
      //printf("Posicao de escrita do contador: %ld\n", ftell(binf)); debugging
      fwrite (&strct, sizeof (unsigned int), 1, binf);//escreve o contador no ficheiro iniciado a 1
      //printf ("Written for the first time!\n");
   }
   else {
      //printf ("Counter: %d\n", strct); debugging
      strct += 1; //caso já exista o contador, incrementa 1
      //printf ("Counter: %d\n", strct); debugging
      fseek (binf, -sizeof (unsigned int), SEEK_CUR);//volta ao início do ficheiro depois do fread do if o ter colocado pronto a escrever a seguir ao contador
      //printf("Posicao de escrita do contador: %ld\n", ftell(binf)); debugging
      fwrite (&strct, sizeof (unsigned int), 1, binf);//escreve o contador incrementado
      //printf ("Writen %d structs\n", strct); debugging
   }
   
   fseek (binf, sizeof (PlData)*(strct - 1), SEEK_CUR);//move-se// para o fim da última estrutura
   printf("Posicao de escrita da struct: %ld\n", ftell(binf)); //debugging
   w_el = fwrite (&pl_data, sizeof (PlData), 1, binf); //escreve a informação da estrutura no file
   fclose (binf);

   printf ("Escreveu %d elementos!\n", w_el);

   //getchar ();
   return 0;
}
