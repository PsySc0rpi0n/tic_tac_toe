#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>

#ifdef _WIN32
# define CLEAR_SCREEN system ("cls")
#else
# define CLEAR_SCREEN puts("\x1b[H\x1b[2J")
#endif

#define CLEAR_INPUT while (getchar ()!= '\n') /*void*/

#define BUFFER_SIZE 256

// comentar a seguinte linha para usar o scanf em vez do fgets
#define USE_FGETS

#define DIM 3

#define PL_BIN_FILE "player_data.bin"

#define MAX_NOM 16
#define MAX_USER 56
#define MAX_GEN 10

typedef struct {
   char nome_jog [MAX_NOM];
   char nome_user [MAX_USER];
   char genero [MAX_GEN];
   int pvic;
   int pemp;
   int pder;
   struct datan{
      int dia;
      int mes;
      int ano;
   } data_nasc;
} PlData;

int matrix[DIM][DIM];
Pldata *pldatatmp;

/*===========================================================================================*
 * *  Protótipos das funções até agora usadas                                                   *
 * *============================================================================================*/

void m_menu();
void reg_jogador();
void reg_jog();
void mostra_info();
void apaga_jogador();
void jogar();
void list_pont();
void jog_jj();
void jog_jc();
void jog_cc();
void jog_mc();
void jog_ac();
void final_jogo();

PlData PedeDadosJogador();
void GravaDadosBin(PlData pldatas, FILE *pfile, unsigned int strct);
PlData *LeDadosBin(unsigned int *strct, FILE *binf, PlData *pldatatmp);
int ProcuraDadosJogador(PlData *pldatatmps, PlData pl_datas, FILE *binf, unsigned int strct);

char m_menu_opcao(char *op);

void iniciar_matrix();
void coord();
void coord1_pc(int *n1,int *n2);
void coord2_pc();
int verifica_vencedor();
void apresenta_matrix();
void Random(int *m1, int *m2);

/*===========================================================================================*
 * *  Fim                                                                                       *
 * *============================================================================================*/

