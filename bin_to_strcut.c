#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_NOM 16
#define MAX_USER 56
#define MAX_GEN 10

#define PL_BIN_FILE "player_data.bin"
#define MAX_PLAYERS 1000

typedef struct {
   char nome_jog [MAX_NOM];
   char nome_user [MAX_USER];
   char genero [MAX_GEN];
   int pvic;
   int pdemp;
   int pder;
   struct datan{
      int dia;
      int mes;
      int ano;
   } data_nasc;
} PlData;

int main (void) {

   FILE *binf;
   int el;
   unsigned int strct, i;
   PlData *pldatatmp;
   
   if ((binf = fopen (PL_BIN_FILE, "rb")) == NULL) {
      printf ("Erro ao abrir o ficheiro!\n");
      exit (0);
   }
   
   fread (&strct, sizeof (unsigned int), 1, binf);
   
   printf ("Struct:%u\n", strct);

   if ((pldatatmp = malloc (strct*sizeof (*pldatatmp))) == NULL) {
      printf ("Erro de memória!\n");
      exit (0);
   }

   for (i = 0; i < strct; i++){
      fread ((pldatatmp + i), sizeof (*pldatatmp), 1, binf);
      printf ("%u\n", i);
   }
   printf ("%d elementos lidos!\n", i);
   
   /*   
   printf ("Qual o elemento que deseja ver?\n");
   scanf ("%d", &el);
   el += 1;
   printf ("A informação pedida é:\n");
   */
   for (i = 0; i < strct; i++) {
   printf ("Nome do jogador: %s\n", pldatatmp [i].nome_jog);
   printf ("O nickname é: %s\n", pldatatmp [i].nome_user);
   printf ("O género do jogador é: %s\n", pldatatmp [i].genero);
   printf ("A data de nascimento é: %d-%d-%d\n", pldatatmp [i].data_nasc.dia, pldatatmp [i].data_nasc.mes, pldatatmp [i].data_nasc.ano);
   }

   fclose (binf);
   getchar ();
   return 0;
}
